### Map
>Map对象保存键值对，并且能够记住键的原始插入顺序。任何值（对象或者原始值）都可以作为键或值。
1. 初始化map对象：
```
var m = new Map();
```
2. 初始化map时传入数据，它默认接受二维数组。
```
var m = new Map([['name','定海露'],['age',18]]);
```
3. 插入数据
```
m.set('name','长草')
```
4. 根据key获取value值
```
var name = m.get('name');
console.log(name);
```
5. 删除某个值
```
m.delete('name')
```
6. 判断某个值是否存在
```
m.has('name');//返回bool值
```
一个key只能对应一个value值，多次对一个key放入value，后面的值会把前面的值覆盖
### Set
1. 将元素添加到Set
```
var s = new Set();
s.add(5);
console.log(s);//set(1){5}
```
2. 从set中删除元素
```
s.delete(5);
console.log(s);//Set(0){}
```
3. 判断某个值是否存在
```
s.has(2);//false
```