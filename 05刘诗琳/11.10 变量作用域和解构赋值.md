## 变量提升
>是指在 JavaScript 代码执行之前，变量和函数声明会被提升到它们所在作用域的顶部。这意味着您可以在声明变量或函数之前使用它们。

以下是一个关于变量提升的举例：
```
console.log(myVar);
var myVar = 10;
```
尽管myVar的声明在console.log()之后，但由于变量提升，myVar被提升到了作用域的顶部。因此，在运行时，代码实际上类似于：
```
var myVar;
console.log(myVar); // 输出：undefined
myVar = 10;
```
## 解构赋值
>JavaScript 中最常用的两种数据结构是 Object 和 Array。
- 对象是一种根据键存储数据的实体。
- 数组是一种直接存储数据的有序列表。
但是，当我们把它们传递给函数时，函数可能不需要整个对象/数组，而只需要其中一部分。

解构赋值 是一种特殊的语法，它使我们可以将数组或对象“拆包”至一系列变量中。有时这样做更方便。
- 数组解构
```
// 我们有一个存放了名字和姓氏的数组
let arr = ["John", "Smith"]

// 解构赋值
// 设置 firstName = arr[0]
// 以及 surname = arr[1]
let [firstName, surname] = arr;

alert(firstName); // John
alert(surname);  // Smith
```
我们可以使用这些变量而非原来的数组项了。
>解构并不意味着破坏

以下两种写法是等价的
```
// let [firstName, surname] = arr;
let firstName = arr[0];
let surname = arr[1];
```
- 对象解构
```
let options = {
  title: "Menu",
  width: 100,
  height: 200
};

let {title, width, height} = options;

alert(title);  // Menu
alert(width);  // 100
alert(height); // 200
```