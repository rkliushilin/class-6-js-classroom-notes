## 方法

```js
   <script>
        var xiaoai = {
            name: 'xiaoai',
            birth: 2004,
            age: function () {
                var y = new Date().getFullYear();
                return y - this.birth;//this特殊变量始终指向当前对象，通过this.birth拿到当前对象属性
            }
        }
        console.log(xiaoai.age());//返回19
    </script>
```

```js
    <script>
        function getAge() {
            var y = new Date().getFullYear();
            return y - this.birth;
        }
        var xiaoai = {
            name: 'xiaoai',
            birth: 1990,
            age: getAge //引用函数
        }
        console.log(xiaoai.age());//返回33
    </script>
```

如果以对象的方法形式调用，比如xiaoming.age()，该函数的this指向被调用的对象，也就是xiaoming，这是符合我们预期的。

如果单独调用函数，比如getAge()，此时，该函数的this指向全局对象，也就是window

```js
解决方法1
<script>
var xiaoming = {
    name: '小明',
    birth: 1990,
    age: function () {
        var that = this; // 在方法内部一开始就捕获this,可以放心地在方法内部定义其他函数
        function getAgeFromBirth() {
            var y = new Date().getFullYear();
            return y - that.birth; // 用that而不是this
        }
        return getAgeFromBirth();
    }
};
console.log(xiaoming.age());
</script>
```
```js
解决方法2
用apply修复getAge()调用：

    function getAge() {
        var y = new Date().getFullYear();
        return y - this.birth;
    }
    
    var xiaoming = {
        name: '小明',
        birth: 1990,
        age: getAge
    };
    
    xiaoming.age(); // 25
    getAge.apply(xiaoming, []); // 25, this指向xiaoming, 参数为空
```

```js
解决方法3
另一个与apply()类似的方法是call()，唯一区别是：

* apply()把参数打包成Array再传入；
  
* call()把参数按顺序传入。
  
比如调用Math.max(3, 5, 4)，分别用apply()和call()实现如下：

    Math.max.apply(null, [3, 5, 4]); // 5
    Math.max.call(null, 3, 5, 4); // 5
```